## GNURadio Flowgraphs
---

This repository contains the GNURadio flowgraphs that are going to be used in the Ground Station: 
- *UHF-Band Up* Flowgraph
	- GMSK Modulation
	- BCH Encoding
- *UHF-Band Down* Flowgraph
	- GMSK Demodulation
	- Convolutional Decoding
	- Synchronization
- *S-Band Down* Flowgraph
	- OQPSK Demodulation
	- LDPC Decoding
	- Synchronization
