#include <iostream>
#include <fstream>
#include <cstdint>

constexpr uint8_t SPS = 10;
constexpr uint8_t NumPackets = 40;
constexpr uint8_t PacketLength = 16;
constexpr uint16_t SampleNumber = 2 * PacketLength * SPS * NumPackets;

int main(int argc, char const *argv[]) {
    std::ifstream input;
    input.open(argv[1], std::ios::in);        // Open the input file
    std::fstream output;
    output.open(argv[2], std::ios::out);    // Open the output file

    for (int i = 0; i < SampleNumber / 4; ++i) {
        uint8_t byte[4];
        for (int j = 0; j < 4; ++j) {
            input >> std::noskipws >> byte[j];
        }
        uint32_t temp = byte[3] << 24 | byte[2] << 16 | byte[1] << 8 | byte[0];
        float out = * reinterpret_cast<float *>(&temp);
        output << out << " ";
    }
    input.close();
    output.close();

    return 0;
}
